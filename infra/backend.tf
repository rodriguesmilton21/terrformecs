terraform {
  backend "s3" {
    bucket         = "ecs-poc-tfstate"
    key            = "global/s3/terraform.tfstate"
    dynamodb_table = "ecs-poc-tfstate"
    encrypt        = true
  }
}