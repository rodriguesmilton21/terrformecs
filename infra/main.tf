
variable "AWS_ACCESS_KEY" {
  type = string
}
variable "AWS_SECRET_ACCESS_KEY" {
  type = string
}
variable "GITLAB_REGISTRY" {
  type = string
}
variable "GITLAB_USER" {
  type = string
}
variable "GITLAB_PASSWORD" {
  type = string
}
provider "aws" {
  region     = "eu-west-1"
  access_key = var.AWS_ACCESS_KEY
  secret_key = var.AWS_SECRET_ACCESS_KEY
}
