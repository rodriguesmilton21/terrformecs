resource "aws_ecs_task_definition" "ecs_td" {
  family                   = "ecs-poc-demo"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = 1024
  memory                   = 2048
  execution_role_arn = "${aws_iam_role.ecs_task_execution_role.arn}"
  container_definitions = <<DEFINITION
   [
    {
      "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-group": "/ecs/ecs-poc-demo",
          "awslogs-region": "eu-west-1",
          "awslogs-stream-prefix": "ecs"
        }
      },
      "portMappings": [
        {
          "hostPort": 4000,
          "protocol": "tcp",
          "containerPort": 4000
        }
      ],
      "cpu": 0,
      "environment": [],
      "repositoryCredentials": {
        "credentialsParameter":"${aws_secretsmanager_secret.gitlab_cred.id}"
      },
      "mountPoints": [],
      "volumesFrom": [],
      "image": "${var.GITLAB_REGISTRY}/${var.GITLAB_USER}/dockerpoc:latest",
      "essential": true,
      "name": "ecs-poc-demo"
    }
  ]
DEFINITION
}

resource "aws_security_group" "aws_sg_poc" {
  name   = "ecs-poc-sg"
  vpc_id = aws_vpc.vpc_default.id

  ingress {
    protocol        = "tcp"
    from_port       = 3000
    to_port         = 3000
    security_groups = [aws_security_group.lb.id]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_ecs_cluster" "main" {
  name = "ecs-poc-demo-cluster"
}

resource "aws_ecs_service" "aws_ecs_service" {
  name            = "ecs-poc-demo-service"
  cluster         = aws_ecs_cluster.main.id
  task_definition = aws_ecs_task_definition.ecs_td.arn
  desired_count   = 1
  launch_type     = "FARGATE"
  # iam_role = "${aws_iam_role.service-role-ecs.arn}"
  network_configuration {
    security_groups = [aws_security_group.aws_sg_poc.id]
    subnets         = aws_subnet.private.*.id
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.lb_tg_default.id
    container_name   = "ecs-poc-demo"
    container_port   = 4000
  }

  depends_on = [aws_lb_listener.lb_listener_default]
}


# ECS TASK DEFINITION EXECUTION ROLE ATTACHED WITH GITLAB ACCESS POLICY
#################################START#####################################
resource "aws_iam_role" "ecs_task_execution_role" {
  name                = "ecs-poc-demo-ecs-task-execution-role"
  assume_role_policy  = data.aws_iam_policy_document.ecs-task-assume-role-policy.json
  managed_policy_arns = [aws_iam_policy.gitlab-login-aws.arn, aws_iam_policy.task-exec.arn]
}

resource "aws_iam_policy" "task-exec" {
  name = "AmazonECSTaskExecutionRolePolicy-ecs"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = ["ecr:GetAuthorizationToken",
          "ecr:BatchCheckLayerAvailability",
          "ecr:GetDownloadUrlForLayer",
          "ecr:BatchGetImage",
          "logs:CreateLogStream",
        "logs:PutLogEvents"]
        Effect   = "Allow"
        Resource = "*"
      },
    ]
  })
}

resource "aws_iam_policy" "gitlab-login-aws" {
  name = "gitlab-login-aws"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action   = ["secretsmanager:GetSecretValue"]
        Effect   = "Allow"
        Resource = "${aws_secretsmanager_secret.gitlab_cred.arn}"
      },
    ]
  })
}

data "aws_iam_policy_document" "ecs-task-assume-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]
    effect  = "Allow"
    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}
##########################ECS TASK EXECUTION ROLE END####################################


# ECS SERVICE ROLE POLICY
#################################START#####################################
resource "aws_iam_role" "service-role-ecs" {
  name                = "ecs-poc-demo-ecs-service-role"
  assume_role_policy  = data.aws_iam_policy_document.ecs-service-assume-role-policy.json
  managed_policy_arns = [aws_iam_policy.ecs-service-role-policy.arn]
}

resource "aws_iam_policy" "ecs-service-role-policy" {
  name = "AmazonECSServiceRolePolicy-ecs"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action   = ["ec2:AttachNetworkInterface",
                "ec2:CreateNetworkInterface",
                "ec2:CreateNetworkInterfacePermission",
                "ec2:DeleteNetworkInterface",
                "ec2:DeleteNetworkInterfacePermission",
                "ec2:Describe*",
                "ec2:DetachNetworkInterface",
                "elasticloadbalancing:DeregisterInstancesFromLoadBalancer",
                "elasticloadbalancing:DeregisterTargets",
                "elasticloadbalancing:Describe*",
                "elasticloadbalancing:RegisterInstancesWithLoadBalancer",
                "elasticloadbalancing:RegisterTargets",
                "route53:ChangeResourceRecordSets",
                "route53:CreateHealthCheck",
                "route53:DeleteHealthCheck",
                "route53:Get*",
                "route53:List*",
                "route53:UpdateHealthCheck",
                "servicediscovery:DeregisterInstance",
                "servicediscovery:Get*",
                "servicediscovery:List*",
                "servicediscovery:RegisterInstance",
                "servicediscovery:UpdateInstanceCustomHealthStatus"]
        Effect   = "Allow"
        Resource = "*"
      },
      {
        Action   = ["autoscaling:Describe*"]
        Effect   = "Allow"
        Resource = "*"
      },
      {
        Action   = [  "autoscaling:DeletePolicy",
                "autoscaling:PutScalingPolicy",
                "autoscaling:SetInstanceProtection",
                "autoscaling:UpdateAutoScalingGroup"]
        Effect   = "Allow"
        Resource = "*",
      },
      {
        Action   = [    "autoscaling-plans:CreateScalingPlan",
                "autoscaling-plans:DeleteScalingPlan",
                "autoscaling-plans:DescribeScalingPlans"]
        Effect   = "Allow"
        Resource = "*"
      },
      {
        Action   = [   "cloudwatch:DeleteAlarms",
                "cloudwatch:DescribeAlarms",
                "cloudwatch:PutMetricAlarm"]
        Effect   = "Allow"
        Resource = "arn:aws:cloudwatch:*:*:alarm:*"
      },
      {
        Action   = ["ec2:CreateTags"]
        Effect   = "Allow"
        Resource = "arn:aws:ec2:*:*:network-interface/*"
      },
      {
        Action   = [ "logs:CreateLogGroup",
                "logs:DescribeLogGroups",
                "logs:PutRetentionPolicy"]
        Effect   = "Allow"
        Resource =  "arn:aws:logs:*:*:log-group:/aws/ecs/*"
      },
      {
        Action   = [ "logs:CreateLogStream",
                "logs:DescribeLogStreams",
                "logs:PutLogEvents"]
        Effect   = "Allow"
        Resource = "arn:aws:logs:*:*:log-group:/aws/ecs/*:log-stream:*"
      },
      {
        Action   = ["ssm:DescribeSessions"]
        Effect   = "Allow"
        Resource = "*"
      },
      {
        Action   = ["ssm:StartSession"]
        Effect   = "Allow"
        Resource = [ "arn:aws:ecs:*:*:task/*",
                "arn:aws:ssm:*:*:document/AmazonECS-ExecuteInteractiveCommand"]
      },
      {
        Action   = [ "servicediscovery:CreateHttpNamespace",
                "servicediscovery:CreateService"]
        Effect   = "Allow"
        Resource = "*"
      },
      {
        Action   = "servicediscovery:TagResource"
        Effect   = "Allow"
        Resource = "*"
      },
      {
        Action   = ["servicediscovery:DeleteService"]
        Effect   = "Allow"
        Resource = "*"
      },
      {
        Action   = ["servicediscovery:DiscoverInstances"]
        Effect   = "Allow"
        Resource = "*"
      }
    ]
  })
}


data "aws_iam_policy_document" "ecs-service-assume-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]
    effect  = "Allow"
    principals {
      type        = "Service"
      identifiers = ["ecs.amazonaws.com"]
    }
  }
}
##########################ECS SERVICE ROLE POLICY END####################################
