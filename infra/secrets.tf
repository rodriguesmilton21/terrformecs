resource "aws_secretsmanager_secret" "gitlab_cred" {
  name        = "demo/poc/glcred/5"
  description = "secret to store gitlab credentials for fetching container from registry"
}
locals {
   creds = {
    username = "${var.GITLAB_USER}"
    password = "${var.GITLAB_PASSWORD}"
  }
}
resource "aws_secretsmanager_secret_version" "cred_value" {
  secret_id     = aws_secretsmanager_secret.gitlab_cred.id
  secret_string = jsonencode(local.creds)
}
# variable "gitlab_key_value" {
#   locals {
#   default = {
#     username = var.GITLAB_USER
#     password = var.GITLAB_PASSWORD
#   }
#   }
#   type = map(string)
# }

